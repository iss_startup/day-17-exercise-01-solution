(function () {
    angular
        .module("UploadApp")
        .service("UploadService", UploadService);

    UploadService.$inject = ["$http", "$q", "Upload"];

    function UploadService($http, $q, Upload) {

        var vm = this;

        vm.refreshAlbum = refresh;
        vm.addToAlbum = add;
        vm.deleteAlbum = _delete;

        function refresh() {
            var defer = $q.defer();
            $http.get("/api/album")
                .then(function (results) {
                    defer.resolve(results.data);
                });
            return (defer.promise);
        }

        function add(dataObject) {
            var defer = $q.defer();
            Upload.upload({
                url: '/upload',
                data: dataObject.data
            }).then(function () {
                defer.resolve()
            });


            return (defer.promise);
        }

        function _delete() {
            var defer = $q.defer();
            $http.delete("/api/album")
                .then(function () {
                    defer.resolve()
                });
            return (defer.promise);
        }
    }
})();
